from .front import (
    Ini,
    __doc__,
    load,
    load_existent,
)
from ._meta import VERSION as __version__

__all__ = [
    'Ini',
    'load',
    'load_existent',
]
