import itertools

import hoover

import tests.fw

import inigrep.front


def mkhint_ex(text):
    return {'cls': 'KeypathError', 's': text}


def mkcase(hint=None, hint_ex=None, **kwargs):
    if hint is not None:
        h = {'hint': hint}
    elif hint_ex is not None:
        h = {'hint_ex': hint_ex}
    return {'args': kwargs} | h


def gamehint(raw=False):
    """
    Create oracle hint for games_with_spaces.ini
    """
    ns = [0, 1, 2, 3, 4]
    chars = ['', ' ', '	', ' 	', '	 ']
    for nvec in itertools.product(ns, ns, ns, ns, ns):
        char1, char2, char3, char4, char5 = [chars[n] for n in nvec]
        parts = ['value', char4] + [str(n) for n in nvec]
        if raw:
            parts = [char3] + parts + [char5]
        yield ''.join(parts)


class ValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.values(files, kpath))


class IniValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.load(files).values(kpath))


class UnitIniValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.UnitIni.from_files(files).values(kpath))


class ValuesTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        ValuesDriver,
        IniValuesDriver,
        UnitIniValuesDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            kpath='',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '' is missing period",
            }
        )

        yield mkcase(
            kpath='.bar',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '.bar' is missing section",
            }
        )

        yield mkcase(
            kpath='foo.',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: 'foo.' is missing key",
            }
        )

        yield mkcase(
            kpath='.',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '.' is missing section and key",
            },
        )

        yield mkcase(
            kpath='.',
            files=['tests/data/foo.ini'],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '.' is missing section and key",
            },
        )

        yield mkcase(
            kpath='foo.bar',
            files=['tests/data/foo.ini'],
            hint=[
                'baz1',
                'baz2',
                'baz3',
            ],
        )

        yield mkcase(
            kpath='bar.qux',
            files=['tests/data/foo.ini'],
            hint=[
                'quux',
            ],
        )

        yield mkcase(
            kpath='foo.quux',
            files=['tests/data/foo.ini'],
            hint=[],
        )

        yield mkcase(
            kpath='with.comments.value',
            files=['tests/data/foo.ini'],
            hint=[
                '0',
            ],
        )

        yield mkcase(
            kpath='0.1',
            files=['tests/data/foo.ini'],
            hint=['2'],
        )

        yield mkcase(
            kpath='maybe spaced.spaced key',
            files=['tests/data/games_with_spaces.ini'],
            hint=list(gamehint()),
        )


class RawValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.raw_values(files, kpath))


class IniRawValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.load(files).raw_values(kpath))


class UnitIniRawValuesDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'kpath']

    def fetch(self, files, kpath):
        return list(inigrep.front.UnitIni.from_files(files).raw_values(kpath))


class RawValuesTest(ValuesTest):

    driver_classes = [
        hoover.HintingDriver,
        RawValuesDriver,
        IniRawValuesDriver,
        UnitIniRawValuesDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            kpath='',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '' is missing period",
            },
        )

        yield mkcase(
            kpath='.',
            files=[],
            hint_ex={
                'cls': "KeypathError",
                's': "invalid keypath: '.' is missing section and key",
            },
        )

        yield mkcase(
            kpath='foo.bar',
            files=['tests/data/foo.ini'],
            hint=[
                ' baz1',
                ' baz2',
                ' baz3',
            ],
        )

        yield mkcase(
            kpath='bar.qux',
            files=['tests/data/foo.ini'],
            hint=[
                ' quux',
            ],
        )

        yield mkcase(
            kpath='foo.quux',
            files=['tests/data/foo.ini'],
            hint=[],
        )

        yield mkcase(
            kpath='with.comments.value',
            files=['tests/data/foo.ini'],
            hint=[
                ' 0   # roughly',
            ],
        )

        yield mkcase(
            kpath='0.1',
            files=['tests/data/Foo.ini'],
            hint=[' 2'],
        )

        yield mkcase(
            kpath='maybe spaced.spaced key',
            files=['tests/data/games_with_spaces.ini'],
            hint=list(gamehint(raw=True)),
        )


class ListSectionsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.front.list_sections(files))


class IniListSectionsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.load(files).list_sections())


class UnitIniListSectionsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.front.UnitIni.from_files(files).list_sections())


class ListSectionsTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        ListSectionsDriver,
        IniListSectionsDriver,
        UnitIniListSectionsDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=[],
            hint=[],
        )

        yield mkcase(
            files=['tests/data/foo.ini'],
            hint=[
                'foo',
                'bar',
                '0',
                'emp',
                'with.dots',
                'with.comments',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/foo.ini'],
            hint=[
                'foo',
                'bar',
                '0',
                'emp',
                'with.dots',
                'with.comments',
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'Foo',
                'Bar',
                '0',
                'Emp',
                'With.Dots',
                'With.Comments',
                'foo',
                'bar',
                'emp',
                'with.dots',
                'with.comments',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
                'foo',
                'bar',
                '0',
                'emp',
                'with.dots',
                'with.comments',
                'Foo',
                'Bar',
                'Emp',
                'With.Dots',
                'With.Comments',
            ],
        )


class ListKeypathsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, **args):
        return list(inigrep.front.list_paths(**args))


class IniListKeypathsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.load(files).list_paths())


class UnitIniListKeypathsDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.front.UnitIni.from_files(files).list_paths())


class ListKeypathsTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        ListKeypathsDriver,
        IniListKeypathsDriver,
        UnitIniListKeypathsDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=[],
            hint=[],
        )

        yield mkcase(
            files=['tests/data/foo.ini'],
            hint=[
                'foo.bar',
                'foo.baz',
                'bar.qux',
                '0.1',
                'with.dots.dotty',
                'with.comments.value',
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini'],
            hint=[
                'Foo.Bar',
                'Foo.Baz',
                'Bar.Qux',
                '0.1',
                'With.Dots.Dotty',
                'With.Comments.Value',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/foo.ini'],
            hint=[
                'foo.bar',
                'foo.baz',
                'bar.qux',
                '0.1',
                'with.dots.dotty',
                'with.comments.value',
                'foo.renegade',   # TODO: not sure we want this, really
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'Foo.Bar',
                'Foo.Baz',
                'Bar.Qux',
                '0.1',
                'With.Dots.Dotty',
                'With.Comments.Value',
                'Foo.renegade',   # TODO: not sure we want this, really
                'foo.bar',
                'foo.baz',
                'bar.qux',
                'with.dots.dotty',
                'with.comments.value',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
                'foo.bar',
                'foo.baz',
                'bar.qux',
                '0.1',
                'with.dots.dotty',
                'with.comments.value',
                'foo.Renegade',   # TODO: not sure we want this, really
                'Foo.Bar',
                'Foo.Baz',
                'Bar.Qux',
                'With.Dots.Dotty',
                'With.Comments.Value',
            ],
        )


class ListKeysDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'section']

    def fetch(self, files, section):
        return list(inigrep.front.list_keys(files, section))


class IniListKeysDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'section']

    def fetch(self, files, section):
        return list(inigrep.load(files).list_keys(section))


class UnitIniListKeysDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'section']

    def fetch(self, files, section):
        return list(inigrep.front.UnitIni.from_files(files).list_keys(section))


class ListKeysTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        ListKeysDriver,
        IniListKeysDriver,
        UnitIniListKeysDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            section=[''],
            files=[],
            hint_ex=mkhint_ex(
                "invalid type: section must be str, got list"
            ),
        )

        yield mkcase(
            section='',
            files=[],
            hint_ex=mkhint_ex(
                "section must not be empty: ''",
            ),
        )

        yield mkcase(
            section='nonexistent',
            files=['tests/data/foo.ini'],
            hint=[
            ],
        )

        yield mkcase(
            section='emp',
            files=['tests/data/foo.ini'],
            hint=[
            ],
        )

        yield mkcase(
            section='nonexistent',
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
            ],
        )

        yield mkcase(
            section='non.existent',
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
            ],
        )

        yield mkcase(
            section='foo',
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
                'bar',
                'baz',
                'Renegade',       # TODO: not sure we want this, really
            ],
        )

        yield mkcase(
            section='foo',
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'bar',
                'baz',
            ],
        )

        yield mkcase(
            section='Foo',
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'Bar',
                'Baz',
                'renegade',
            ],
        )

        yield mkcase(
            section='bar',
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'qux',
            ],
        )

        yield mkcase(
            section='Bar',
            files=['tests/data/Foo.ini'],
            hint=[
                'Qux',
            ],
        )

        yield mkcase(
            section='With.Dots',
            files=['tests/data/foo.ini', 'tests/data/foo.ini'],
            hint=[
            ],
        )

        yield mkcase(
            section='With.Dots',
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
                'Dotty',
            ],
        )

        yield mkcase(
            section='With.Dots',
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                'Dotty',
            ],
        )

        yield mkcase(
            section='With.Dots',
            files=['tests/data/Foo.ini', 'tests/data/Foo.ini'],
            hint=[
                'Dotty',
            ],
        )


class FullCloneDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.front.clone(files))


class IniFullCloneDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.load(files).clone())


class UnitIniFullCloneDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files):
        return list(inigrep.front.UnitIni.from_files(files).clone())


class FullCloneTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        FullCloneDriver,
        IniFullCloneDriver,
        UnitIniFullCloneDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=[],
            hint=[],
        )

        yield mkcase(
            files=['tests/data/foo.ini'],
            hint=[
                '',
                '[foo]',
                '    bar = baz1',
                '    baz = heya',
                '    bar = baz2',
                '',
                '[bar]',
                '    qux = quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[emp]',
                '',
                '[with.dots]',
                '    dotty = polka',
                '',
                '[with.comments]',
                '    value = 0   # roughly',
                '',
                '[foo]',
                '    bar = baz3',
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini'],
            hint=[
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Bar]',
                '    Qux = Quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[Emp]',
                '',
                '[With.Dots]',
                '    Dotty = Polka',
                '',
                '[With.Comments]',
                '    Value = 0   ; Precisely',
                '',
                '[Foo]',
                '    Bar = Baz3',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/foo.ini'],
            hint=[
                '',
                '[foo]',
                '    bar = baz1',
                '    baz = heya',
                '    bar = baz2',
                '',
                '[bar]',
                '    qux = quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[emp]',
                '',
                '[with.dots]',
                '    dotty = polka',
                '',
                '[with.comments]',
                '    value = 0   # roughly',
                '',
                '[foo]',
                '    bar = baz3',
                '    renegade = sectionless key',   # TODO: don't want this
                '',
                '[foo]',
                '    bar = baz1',
                '    baz = heya',
                '    bar = baz2',
                '',
                '[bar]',
                '    qux = quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[emp]',
                '',
                '[with.dots]',
                '    dotty = polka',
                '',
                '[with.comments]',
                '    value = 0   # roughly',
                '',
                '[foo]',
                '    bar = baz3',
            ],
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint=[
                '',
                '[foo]',
                '    bar = baz1',
                '    baz = heya',
                '    bar = baz2',
                '',
                '[bar]',
                '    qux = quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[emp]',
                '',
                '[with.dots]',
                '    dotty = polka',
                '',
                '[with.comments]',
                '    value = 0   # roughly',
                '',
                '[foo]',
                '    bar = baz3',
                '    Renegade = Sectionless Key',   # TODO: don't want this
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Bar]',
                '    Qux = Quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[Emp]',
                '',
                '[With.Dots]',
                '    Dotty = Polka',
                '',
                '[With.Comments]',
                '    Value = 0   ; Precisely',
                '',
                '[Foo]',
                '    Bar = Baz3',
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint=[
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Bar]',
                '    Qux = Quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[Emp]',
                '',
                '[With.Dots]',
                '    Dotty = Polka',
                '',
                '[With.Comments]',
                '    Value = 0   ; Precisely',
                '',
                '[Foo]',
                '    Bar = Baz3',
                '    renegade = sectionless key',   # TODO: don't want this
                '',
                '[foo]',
                '    bar = baz1',
                '    baz = heya',
                '    bar = baz2',
                '',
                '[bar]',
                '    qux = quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[emp]',
                '',
                '[with.dots]',
                '    dotty = polka',
                '',
                '[with.comments]',
                '    value = 0   # roughly',
                '',
                '[foo]',
                '    bar = baz3',
            ],
        )

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/Foo.ini'],
            hint=[
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Bar]',
                '    Qux = Quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[Emp]',
                '',
                '[With.Dots]',
                '    Dotty = Polka',
                '',
                '[With.Comments]',
                '    Value = 0   ; Precisely',
                '',
                '[Foo]',
                '    Bar = Baz3',
                '    Renegade = Sectionless Key',   # TODO: don't want this
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Bar]',
                '    Qux = Quux',
                '',
                '[0]',
                '    1 = 2',
                '',
                '[Emp]',
                '',
                '[With.Dots]',
                '    Dotty = Polka',
                '',
                '[With.Comments]',
                '    Value = 0   ; Precisely',
                '',
                '[Foo]',
                '    Bar = Baz3',
            ],
        )


class PartialCloneByKpathDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, **args):
        return list(inigrep.front.clone(**args))


class IniPartialCloneByKpathDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files, **args):
        return list(inigrep.load(files).clone(**args))


class PartialCloneByKpathTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        PartialCloneByKpathDriver,
        IniPartialCloneByKpathDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/Foo.ini'],
            kpath='Foo.',
            hint=[
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Foo]',
                '    Bar = Baz3',
                '    Renegade = Sectionless Key',   # TODO: don't want this
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Foo]',
                '    Bar = Baz3',
            ],
        )


class UnitIniPartialCloneBySectionDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files', 'section']

    def fetch(self, files, section):
        return list(inigrep.front.UnitIni.from_files(files).clone_section(section))


class PartialCloneBySectionTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        UnitIniPartialCloneBySectionDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/Foo.ini'],
            section='Foo',
            hint=[
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Foo]',
                '    Bar = Baz3',
                '    Renegade = Sectionless Key',   # TODO: don't want this
                '',
                '[Foo]',
                '    Bar = Baz1',
                '    Baz = Heya',
                '    Bar = Baz2',
                '',
                '[Foo]',
                '    Bar = Baz3',
            ],
        )


class IniDataDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files, **args):
        return inigrep.load(files).data()


class UnitIniDataDriver(tests.fw.InigrepDriver):

    mandatory_args = ['files']

    def fetch(self, files, **args):
        return inigrep.front.UnitIni.from_files(files).data(**args)


class IniDataTest(tests.fw.InigrepTestCase):

    driver_classes = [
        hoover.HintingDriver,
        IniDataDriver,
        UnitIniDataDriver,
    ]

    @property
    def cases(self):

        yield mkcase(
            files=[],
            hint={},
        )

        yield mkcase(
            files=['tests/data/foo.ini'],
            hint={
                '0.1': [
                    '2',
                ],
                'bar.qux': [
                    'quux',
                ],
                'foo.bar': [
                    'baz1',
                    'baz2',
                    'baz3',
                ],
                'foo.baz': [
                    'heya',
                ],
                'with.comments.value': [
                    '0',
                ],
                'with.dots.dotty': [
                    'polka',
                ],
            },
        )

        yield mkcase(
            files=['tests/data/Foo.ini'],
            hint={
                '0.1': [
                    '2',
                ],
                'Bar.Qux': [
                    'Quux',
                ],
                'Foo.Bar': [
                    'Baz1',
                    'Baz2',
                    'Baz3',
                ],
                'Foo.Baz': [
                    'Heya',
                ],
                'With.Comments.Value': [
                    '0',
                ],
                'With.Dots.Dotty': [
                    'Polka',
                ],
            },
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/foo.ini'],
            hint={
                '0.1': [
                    '2',
                    '2',
                ],
                'bar.qux': [
                    'quux',
                    'quux',
                ],
                'foo.bar': [
                    'baz1',
                    'baz2',
                    'baz3',
                    'baz1',
                    'baz2',
                    'baz3',
                ],
                'foo.baz': [
                    'heya',
                    'heya',
                ],
                'foo.renegade': [
                    'sectionless key'
                ],
                'with.comments.value': [
                    '0',
                    '0',
                ],
                'with.dots.dotty': [
                    'polka',
                    'polka',
                ],
            },
        )

        yield mkcase(
            files=['tests/data/foo.ini', 'tests/data/Foo.ini'],
            hint={
                '0.1': [
                    '2',
                    '2',
                ],
                'bar.qux': [
                    'quux',
                ],
                'foo.bar': [
                    'baz1',
                    'baz2',
                    'baz3',
                ],
                'foo.baz': [
                    'heya',
                ],
                'foo.Renegade': [
                    'Sectionless Key'
                ],
                'with.comments.value': [
                    '0',
                ],
                'with.dots.dotty': [
                    'polka',
                ],
                'Bar.Qux': [
                    'Quux',
                ],
                'Foo.Bar': [
                    'Baz1',
                    'Baz2',
                    'Baz3',
                ],
                'Foo.Baz': [
                    'Heya',
                ],
                'With.Comments.Value': [
                    '0',
                ],
                'With.Dots.Dotty': [
                    'Polka',
                ],
            },
        )

        yield mkcase(
            files=['tests/data/Foo.ini', 'tests/data/foo.ini'],
            hint={
                '0.1': [
                    '2',
                    '2',
                ],
                'bar.qux': [
                    'quux',
                ],
                'foo.bar': [
                    'baz1',
                    'baz2',
                    'baz3',
                ],
                'foo.baz': [
                    'heya',
                ],
                'with.comments.value': [
                    '0',
                ],
                'with.dots.dotty': [
                    'polka',
                ],
                'Bar.Qux': [
                    'Quux',
                ],
                'Foo.Bar': [
                    'Baz1',
                    'Baz2',
                    'Baz3',
                ],
                'Foo.Baz': [
                    'Heya',
                ],
                'Foo.renegade': [
                    'sectionless key'
                ],
                'With.Comments.Value': [
                    '0',
                ],
                'With.Dots.Dotty': [
                    'Polka',
                ],
            },
        )
