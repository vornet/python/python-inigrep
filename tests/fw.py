
import hoover

import inigrep.core


class InigrepDriver(hoover.Driver):

    valid_exceptions = [inigrep.core.KeypathError]

    def cook_ex(self, raw_ex, args):
        return {
            'cls': raw_ex.__class__.__name__,
            's': str(raw_ex),
        }


class InigrepTestCase(hoover.Case):

    yaml_file = 'hoover.yaml'

    def test_all(self):
        self.run_plan(self.cases)
