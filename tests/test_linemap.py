from inigrep.front import LineMap, MissingKeyError, RepeatKeyError

import pytest


def test_linemap_invalid_data():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data=1)
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but data is int'
    )


def test_linemap_invalid_key():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data={1: []})
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but key is int'
    )


def test_linemap_invalid_value():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data={'foo.bar': 1})
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but value is int'
    )


def test_linemap_empty_key():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data={'': []})
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but key is empty'
    )


def test_linemap_empty_value():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data={'foo.bar': []})
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but value is empty'
    )


def test_linemap_periodless_key():
    with pytest.raises(ValueError) as excinfo:
        LineMap(data={'foo': []})
    assert excinfo.type is ValueError
    assert str(excinfo.value) == (
        'invalid data type: '
        'expected dict of strings to lists of strings but key does not contain period: \'foo\''
    )


def test_linemap_empty_data():
    data = {}
    lm = LineMap(data=data)

    #
    # checkers
    #
    assert not lm.have('foo.bar')

    #
    # value getters
    #
    assert lm.get01('foo.bar') is None
    assert lm.get0N('foo.bar') == []
    with pytest.raises(MissingKeyError) as excinfo_get0N:
        lm.get11('foo.bar')
    assert excinfo_get0N.type is MissingKeyError
    assert str(excinfo_get0N.value) == 'foo.bar'
    with pytest.raises(MissingKeyError) as excinfo_get1N:
        lm.get1N('foo.bar')
    assert excinfo_get1N.type is MissingKeyError
    assert str(excinfo_get1N.value) == 'foo.bar'

    #
    # section finder
    #
    assert lm.sfind() == []
    assert lm.sfind('foo') == []
    assert lm.slist() == []
    assert lm.slist('foo') == []

    #
    # key finder
    #
    assert lm.kfind() == []
    assert lm.kfind('foo') == []
    assert lm.klist('foo') == []


def test_linemap_single():
    data = {
        'foo.bar': ['a'],
        # [foo]
        #    bar = a
    }
    lm = LineMap(data=data)

    #
    # checkers
    #
    assert lm.have('foo.bar')

    #
    # value getters
    #
    assert lm.get01('foo.bar') == 'a'
    assert lm.get0N('foo.bar') == ['a']
    assert lm.get11('foo.bar') == 'a'
    assert lm.get1N('foo.bar') == ['a']

    #
    # section finder
    #
    assert lm.sfind() == ['foo']
    assert lm.sfind('x') == []
    assert lm.slist() == ['foo']
    assert lm.slist('x') == []

    #
    # key finder
    #
    assert lm.kfind() == ['foo.bar']
    assert lm.kfind('foo') == ['bar']
    assert lm.kfind('x') == []
    assert lm.klist('foo') == ['bar']
    assert lm.klist('x') == []


def test_linemap_multi():
    data = {
        'foo.bar': ['a', 'b'],
        # [foo]
        #    bar = a
        #    bar = b
    }
    lm = LineMap(data=data)

    #
    # checkers
    #
    assert lm.have('foo.bar')

    #
    # value getters
    #
    with pytest.raises(RepeatKeyError) as excinfo:
        lm.get01('foo.bar') == 'a'
    assert excinfo.type is RepeatKeyError
    assert str(excinfo.value) == 'foo.bar'
    assert lm.get0N('foo.bar') == ['a', 'b']
    with pytest.raises(RepeatKeyError) as excinfo:
        lm.get11('foo.bar') == 'a'
    assert excinfo.type is RepeatKeyError
    assert lm.get1N('foo.bar') == ['a', 'b']

    #
    # section finder
    #
    assert lm.sfind() == ['foo']
    assert lm.sfind('x') == []
    assert lm.slist() == ['foo']
    assert lm.slist('x') == []

    #
    # key finder
    #
    assert lm.kfind() == ['foo.bar']
    assert lm.kfind('foo') == ['bar']
    assert lm.kfind('x') == []
    assert lm.klist('foo') == ['bar']
    assert lm.klist('x') == []


def test_linemap_subsections():
    data = {
        'foo.bar.baz': ['a'],
        'foo.bar.hey': ['ho'],
        'foo.bar.two.baz': ['b'],
        'foo.bax.baz': ['c'],
        'quux.bax.baz': ['d'],
        'joe.baz': ['e'],
        # [foo.bar]
        #    baz = a
        #    hey = ho
        # [foo.bar.two]
        #    baz = b
        # [foo.bax]
        #    baz = c
        # [quux.bax]
        #    baz = d
        # [joe
        #    baz = e
    }
    lm = LineMap(data=data)

    #
    # checkers
    #
    assert lm.have('foo.bar.baz')

    #
    # value getters
    #
    assert lm.get01('foo.bar.baz') == 'a'
    assert lm.get0N('foo.bar.baz') == ['a']
    assert lm.get11('foo.bar.baz') == 'a'
    assert lm.get1N('foo.bar.baz') == ['a']
    assert lm.get01('foo.bar.hey') == 'ho'
    assert lm.get0N('foo.bar.hey') == ['ho']
    assert lm.get11('foo.bar.hey') == 'ho'
    assert lm.get1N('foo.bar.hey') == ['ho']
    assert lm.get01('foo.bar.two.baz') == 'b'
    assert lm.get0N('foo.bar.two.baz') == ['b']
    assert lm.get11('foo.bar.two.baz') == 'b'
    assert lm.get1N('foo.bar.two.baz') == ['b']
    assert lm.get01('foo.bax.baz') == 'c'
    assert lm.get0N('foo.bax.baz') == ['c']
    assert lm.get11('foo.bax.baz') == 'c'
    assert lm.get1N('foo.bax.baz') == ['c']
    assert lm.get01('quux.bax.baz') == 'd'
    assert lm.get0N('quux.bax.baz') == ['d']
    assert lm.get11('quux.bax.baz') == 'd'
    assert lm.get1N('quux.bax.baz') == ['d']
    assert lm.get01('joe.baz') == 'e'
    assert lm.get0N('joe.baz') == ['e']
    assert lm.get11('joe.baz') == 'e'
    assert lm.get1N('joe.baz') == ['e']

    #
    # section finder
    #
    assert lm.sfind() == ['foo.bar', 'foo.bar.two', 'foo.bax', 'quux.bax', 'joe']
    assert lm.sfind('foo') == ['bar', 'bar.two', 'bax']
    assert lm.sfind('quux') == ['bax']
    assert lm.sfind('joe') == []
    assert lm.sfind('x') == []
    assert lm.slist() == ['foo', 'quux', 'joe']
    assert lm.slist('foo') == ['bar', 'bax']
    assert lm.slist('quux') == ['bax']
    assert lm.slist('x') == []

    #
    # key finder
    #
    assert lm.kfind() == ['foo.bar.baz', 'foo.bar.hey', 'foo.bar.two.baz', 'foo.bax.baz', 'quux.bax.baz', 'joe.baz']
    assert lm.kfind('foo') == ['bar.baz', 'bar.hey', 'bar.two.baz', 'bax.baz']
    assert lm.kfind('quux') == ['bax.baz']
    assert lm.kfind('joe') == ['baz']
    assert lm.kfind('x') == []
    assert lm.klist('foo.bar') == ['baz', 'hey']
    assert lm.klist('quux') == []
    assert lm.klist('joe') == ['baz']
    assert lm.klist('x') == []
